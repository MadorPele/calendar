import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import easySpinner from 'vue-easy-spinner'
import VWave from 'v-wave'

createApp(App).use(store).use(router).use(easySpinner, {
  prefix: 'easy'
}).use(VWave).mount('#app')
