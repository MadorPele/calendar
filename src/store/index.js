import {
  createStore
} from 'vuex'

export default createStore({
  state: {
    historyCalendars: []
  },
  getters: {
  },
  actions: {
  },
  mutations: {
    toggleCalendar(state, { mador, year }) {
      let calendar = state.historyCalendars.find( cal => cal.mador == mador && cal.year == year )
      calendar.selected = !calendar.selected
    }
  },
  modules: {}
})