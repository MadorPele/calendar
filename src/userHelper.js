const { default: axios } = require("axios");

let allUsers = null
async function fetchUsers() {
  if (allUsers) return allUsers
  let res = await axios.get('users.json')
  allUsers = res.data
  return res.data
}

export async function getUser(username) {
  let users = await fetchUsers()
  return users.find(user => user.name == username)
}

export async function getMadors(username) {
  let users = await fetchUsers()
  let madorsString = users.find(user => user.name == username).madors
  let madors = []
  madorsString.split(',').forEach(mador => {
    madors.push([mador.split('-')[0].trim(), mador.split('-')[1].trim()])
  })
  return madors
} 
