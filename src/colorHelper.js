// for status color
function getStatusColor({type, name}) {
  if (type == 'CALENDAR') {
    switch (name) {
      case 'בוצע': return '#428c42'
      case 'בביצוע':  return '#aeae54'
      case 'חורג': return '#c60e0e'
      case 'בתכנון': return '#007bff'
      default: return '#333'
    }
  } else if (type == 'LIST') {
    switch (name) {
      case 'בוצע': return 'rgb(121, 173, 121)'
      case 'בביצוע': return'#d3d36c'
      case 'חורג': return 'rgb(255, 148, 148)'
      case 'בתכנון': return '#72a5e6'
      default: return '#333'
    }
  }
}

// for mador colors
const savedColors = [
  {
    mador: 'מדור פלא',
    // color: 'green'
    color: 'blue'
  },
  {
    mador: 'מדור מפקדות ותחקור',
    // color: '#d3d35d'
    color: 'purple'
  },
  {
    mador: 'מדור פ"ה',
    color: 'green'
  },
  {
    mador: 'מדור חילוץ',
    color: 'orange'
  },
  {
    mador: 'מדור תורה ועריכה',
    color: 'lightblue'
  },
]

function getHash(input){
  var hash = 0, len = input.length;
  for (var i = 0; i < len; i++) {
    hash  = ((hash << 5) - hash) + input.charCodeAt(i);
    hash |= 0; // to 32bit integer
  }
  return hash
}

function getMadorColor(name) {
  const savedColor = savedColors.find(item => item.mador == name)
  if (savedColor) return savedColor.color
  else return `hsl(${getHash(name)}, 50%, 50%)`
}

// export functions
export {getStatusColor, getMadorColor}