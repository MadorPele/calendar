import { createRouter, createWebHashHistory } from 'vue-router'

import Home from '../views/Home.vue'
import CalendarsPage from '../views/CalendarsPage.vue'
import ListsPage from '../views/ListsPage.vue'

const routes = [
  {
    path: '/',
    redirect: '/home'
  }, 
  {
    path: '/home',
    name: 'home',
    component: Home
  },
  {
    path: '/calendars',
    name: 'calendars',
    component: CalendarsPage
  },
  {
    path: '/lists',
    name: 'lists',
    component: ListsPage
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
