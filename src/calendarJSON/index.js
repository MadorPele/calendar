// color helper
import { getStatusColor, getMadorColor } from '../colorHelper'

// Hebrew Months and Days Names
const monthNames = ["ינואר", "פברואר", "מרץ", "אפריל", "מאי", "יוני",
"יולי", "אוגוסט", "ספטמבר", "אוקטובר", "נובמבר", "דצמבר"
]
const dayNames = ["ראשון", "שני", "שלישי", "רביעי", "חמישי", "שישי", "שבת"]

// Date Helper Functions
function daysInMonth (month, year) {
  return new Date(year, month, 0).getDate();
}
function getDay (day, month, year) {
  return new Date(year, month-1, day).getDay();
}
function addDaysToDate(date, days) {
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}

import axios from 'axios'

let calendars = []

export default function fetchCalendar(mador, year) {

  return new Promise(resolve =>{
    
    if (calendars[`${mador} - ${year}`] != undefined) {
      resolve (calendars[`${mador} - ${year}`]);
    } 
    
    else {
  
      year = parseInt(year)
      
      let calendarJSON = []
      let tasksJSON = []
    
      let months = []
      for (let month = 1; month <= 12; month++) {
        months.push([month, year]);
      }
    
      axios.get(`${mador} - ${year}.json`)
      .then(response => {

        // On Army: Convert xml respone to JSON here

        tasksJSON = response.data
        
        for (let index in months) {
    
          // Create month json
          calendarJSON[index] = {
            name: `${monthNames[months[index][0]-1]} ${months[index][1]}`,
            month: months[index][0],
            year: months[index][1],
            days: {},
            tasks: [],
            firstWeekDay: new Date(months[index][1], months[index][0]-1, 1).getDay()+1,
            currMonth: false
          }
          
          // Create day json
          for (let i = 0; i < daysInMonth(months[index][0], months[index][1]); i++) {
            calendarJSON[index].days[i+1] = {
              day: i+1,
              weekDay: getDay(i+1, months[index][0], months[index][1])+1,
              dayName: dayNames[getDay(i+1, months[index][0], months[index][1])],
              month: months[index][0],
              year: months[index][1],
              tasks: [],
              currDay: false
            }
          }
        }

        let madorColor = getMadorColor(mador);
        for (const i in tasksJSON) {
          let task = tasksJSON[i];

          let startDate = new Date(task.startdate);
          let endDate = new Date(task.enddate);
          task.fullstartdate = startDate;
          task.fullenddate = endDate;
          task.startdate = startDate;
          task.enddate = endDate;

          //push task to each month
          let startMonth = startDate.getMonth();
          let endMonth = endDate.getMonth();
          task.color = madorColor;
          task.statusColor = getStatusColor({type:'CALENDAR', name: task.status})
          for (let j = startMonth; j <= endMonth; j++) {
            let tempTask = {...task};
            if (startMonth < endMonth) {
              if (j == startMonth) {
                tempTask.enddate = new Date(startDate.getFullYear(), startMonth+1, 0)
                tempTask.frontArrow = true;
              } else if (j == endMonth) {
                tempTask.startdate = new Date(startDate.getFullYear(), endMonth, 1)
                tempTask.backArrow = true;
              } else if (j > startMonth && j < endMonth){
                tempTask.startdate = new Date(startDate.getFullYear(), endMonth, 1)
                tempTask.enddate = new Date(startDate.getFullYear(), startMonth+1, 0)
                tempTask.frontArrow = true;
                tempTask.backArrow = true;    
              }
            }
            calendarJSON[j].tasks.push(tempTask);
          }
          
          // push task to each day 
          let Difference_In_Time = endDate.getTime() - startDate.getTime();
          let Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
          
          let taskDates = []
          for (let i = 0; i <= Difference_In_Days; i++) {
            let newDate = addDaysToDate(startDate, i);
            taskDates.push([newDate.getMonth(), newDate.getDate()])
          }
    
          for (let date of taskDates) {
            calendarJSON[date[0]].days[date[1]].tasks.push(task)
          }
    
        }

        
        let currentDate = new Date()
        if (calendarJSON[0].year == currentDate.getFullYear()) {
          calendarJSON[currentDate.getMonth()].currMonth = true;
          calendarJSON[currentDate.getMonth()].days[currentDate.getDate()].currDay = true;
        }

        console.log(calendarJSON);

        calendars[`${mador} - ${year}`] = {
          calendar: calendarJSON,
          color: madorColor
        }
        resolve({
          calendar: calendarJSON,
          color: madorColor
        });
      })
    } 
  })
}