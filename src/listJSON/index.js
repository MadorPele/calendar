// color helper
import { getStatusColor } from '../colorHelper'

import axios from 'axios'

let lists = []

export default function fetchList(mador, year) {

  return new Promise(resolve => {

    if (lists[`${mador} - ${year}`] != undefined) {
      resolve(lists[`${mador} - ${year}`]);
    } else {
      axios.get(`${mador} - ${year}.json`)
        .then(response => {
          let listJSON = response.data
          let listArr = Object.values(listJSON)
          for (let item of listArr) {
            item.startdate = new Date(item.startdate)
            item.enddate = new Date(item.enddate)
            item.statusColor = getStatusColor({type:'LIST', name: item.status})
          }
          console.log(listArr)
          lists[`${mador} - ${year}`] = listArr
          resolve(listArr)
        })
    }
  })
}